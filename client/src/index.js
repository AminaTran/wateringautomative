import React from "react";
import ReactDOM from "react-dom";
import App from "./containers/App";
import registerServiceWorker from "./registerServiceWorker";
import "./style/global";
import "./index.css"
import {BrowserRouter as Router} from 'react-router-dom';
import {SensorProvider} from './components/context'
ReactDOM.render(
    <SensorProvider>
    <Router>
    <App />
  </Router>
  </SensorProvider>, 
    document.getElementById("root"));
registerServiceWorker();
