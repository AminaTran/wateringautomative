import React,{ Component} from 'react';
import {Link} from 'react-router-dom';
import wall from './wall.jpg';


class Header extends Component{
    render(){
        return (   
              
            <nav className="navbar navbar-default" role="navigation">
                <div className="navbar-header">
                    
                    <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span className="sr-only">Stephen</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    
                    </button>
                        <Link to='/config'>
                        <a className="navbar-brand" href="#">Config</a>
                        </Link>
                        <Link to='/viewLog'>
                        <a className="navbar-brand" href="#">ViewLog</a>
                        </Link>
                        

                </div>
                <div className="collapse navbar-collapse navbar-ex1-collapse">
                    <ul className="nav navbar-nav navbar-right">
                    
                        <li className="dropdown">
                            <a href="#" className="dropdown-toggle" data-toggle="Stephen">Stephen <b className="caret"></b></a>
                            <ul className="dropdown-menu">
                                <li><a href="#">About me</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
            
            
        );
    }
}

export default Header;
