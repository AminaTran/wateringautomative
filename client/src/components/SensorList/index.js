import React, {Component} from 'react';
import Sensor from '../Sensor';
import storeSensors from '../mongo';
import {SensorConsumer} from '../context';
import openSocket from 'socket.io-client';
import Cookies from 'js-cookie'

class SensorList extends Component {
  state={
    // cookietest: Cookies.get('name'),
    sensors: storeSensors,
    test: []
  };

  componentWillMount(){
    if(Cookies.get('sensorID')){
      Cookies.remove('sensorID');
      Cookies.remove('moisture');
    }
  }
  render (){
    return(
      <div class='container'>
      
      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <a href="#" className="thumbnail">
          <div>
          <SensorConsumer>
            { value => { 
              return value.sensors.map(sensor =>{
                return <Sensor key={sensor.id} sensor={sensor}/>
              });
            }}
            
            </SensorConsumer>
          </div>
        </a>
      </div>

      </div>     
    )
  }
      

}

export default SensorList;
