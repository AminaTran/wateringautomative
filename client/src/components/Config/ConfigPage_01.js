import React, {Component} from 'react';
import './styles.css'
import treeImage from './assets/tree.png'
import data from './assets/data.json'
import Cookies from 'js-cookie'
import { SensorConsumer } from '../context';


const formValid = formErrors => {

    let valid = true

    Object.values(formErrors).forEach( val => {
        val.lenght > 0 && (valid = false) 
    });

    return valid
}

class ConfigPage_01 extends Component {

    getValue(detailSensor){
        if(detailSensor){
            Cookies.set('sensorID', detailSensor.sensorID)
            return {sensorID: detailSensor.sensorID}
        }
        else{
            return {sensorID: Cookies.get('sensorID')}
        }
    }

    constructor(props){
        super(props);
        this.state = {
            sensorId :'',
            plantType : '',
             
            springThreshold : null,
            summerThreshold : null,
            autumnThreshold : null,
            winterThreshold : null,
            formErrors:{
                sensorId :'',
                plantType : '',
                 
                springThreshold : '',
                summerThreshold : '',
                autumnThreshold : '',
                winterThreshold : '',
            }
        }
    }

    handleSubmit = e =>{
        e.preventDefault();

        if(formValid(this.state.formErrors)){
            console.log(`
                -- SUBMITTING --
                Sensor ID: ${this.state.sensorId}
                Plant Type: ${this.state.plantType}
                Spring Threshold: ${this.state.springThreshold}
                Summer Threshold: ${this.state.summerThreshold}
                Autumn Threshold: ${this.state.autumnThreshold}
                Winter Threshold: ${this.state.winterThreshold}
            `)
        }
        else{
            console.error('Form invalid - display message error');
        }

    }

    handleChange = e => {
        e.preventDefault()
        
        const {name, value} = e.target;
        let formErrors = this.state.formErrors;
        console.log(`name  = ${name}, value = ${value}`);


        switch(name)
        {
            case 'springThreshold':
                formErrors.springThreshold =
                value < 1 || value >1024 
                ? "Out of range"
                : ""
                break;
            default:
                break;
        }

        this.setState({formErrors, [name]: value}, () => console.log(this.state) );

    }

    render(){

        const formErrors = this.state.formErrors; 
        const progSensorID = this.props.sensorId;

        
        return (
            <SensorConsumer>
                {value => {
                    
                    const{sensorID}=this.getValue(value.detailSensor);
                    return (
            <div className='wrapper'>
                <div className='form-wrapper'>
                    <h1 style={{marginBottom:'10px'}}>Config Page</h1>

                    <div className='flex-main'>

                        <form onSubmit={this.handleSubmit}  noValidate>
                        <div className='sensorId_plantType'>
                            <div className=''>
                                <label htmlFor='sensorId'>Sensor ID: </label>
                                <input 
                                    type = 'text'
                                    className = ''
                                    placeholder = 'Sensor ID'
                                    name = 'sensorId'
                                    value={sensorID}
                                    noValidate
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className=''>
                                <label htmlFor='plantType'>Plant Type: </label>
                                <input 
                                    type = 'text'
                                    className = ''
                                    placeholder = 'Plant Type'
                                    name = 'plantType'
                                    defaultValue={data.plant_type}
                                    noValidate
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>

                        
                        <div className='concept'>
                            <text>Threshold and amount the water</text>
                        </div>

                        <div className='threshold_amount_water'>
                            <div className='seasons'>
                                <div className='titleThresholdAmount'>Spring</div>
                                <div className='inputThresholdAmount'>
                                    <input 
                                        type = 'number'
                                        min = '0'
                                        max = '1023'
                                        className = ''
                                        placeholder = 'Threshold'
                                        name = 'springThreshold'
                                        defaultValue={data.spring[0]}
                                        onChange={this.handleChange}
                                    />
                                    {formErrors.springThreshold !== null && 
                                    (<span className="errorMessage">{formErrors.springThreshold}</span>)}
                                </div>
                            </div>

                            <div className='seasons'>
                                <div className='titleThresholdAmount'>Summer</div>
                                <div className='inputThresholdAmount'>
                                    <input 
                                        type = 'number'
                                        min = '0'
                                        max = '1023'
                                        className = ''
                                        placeholder = 'Threshold'
                                        name = 'springThreshold'
                                        defaultValue={data.spring[0]}
                                        onChange={this.handleChange}
                                    />
                                    {formErrors.springThreshold !== null && 
                                    (<span className="errorMessage">{formErrors.springThreshold}</span>)}
                                </div>
                            </div>

                            <div className='seasons'>
                                <div className='titleThresholdAmount'>Autumn</div>
                                <div className='inputThresholdAmount'>
                                    <input 
                                        type = 'number'
                                        min = '0'
                                        max = '1023'
                                        className = ''
                                        placeholder = 'Threshold'
                                        name = 'springThreshold'
                                        defaultValue={data.spring[0]}
                                        onChange={this.handleChange}
                                    />
                                    {formErrors.springThreshold !== null && 
                                    (<span className="errorMessage">{formErrors.springThreshold}</span>)}
                                </div>
                            </div>

                            <div className='seasons'>
                                <div className='titleThresholdAmount'>Winter</div>
                                <div className='inputThresholdAmount'>
                                    <input 
                                        type = 'number'
                                        min = '1'
                                        max = '1023'
                                        className = ''
                                        placeholder = 'Threshold'
                                        name = 'springThreshold'
                                        defaultValue={data.spring[0]}
                                        onChange={this.handleChange}
                                    />
                                    {formErrors.springThreshold !== null && 
                                    (<span className="errorMessage">{formErrors.springThreshold}</span>)}
                                </div>
                            </div>

                            
                        </div>

                        <div className='submitApply'>
                            <button type='submit'>Apply</button>
                        </div>

                        </form>

                        <div className='imagePlant'>
                            <img src={treeImage} alt="Plant"/>

                        </div>
                    
                    </div>




                </div>

            </div>
                    )
                }}
            </SensorConsumer>
        )

    }
} 

export default ConfigPage_01;