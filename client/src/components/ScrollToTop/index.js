import React from 'react'
import {useWindowScroll} from "react-use";
import {Button} from 'reactstrap';
ScrollToTop =()=>{
    const {y: pageYOffset}=useWindowScroll();

    return (
        <div className="scroll-to-top cursor-pointer text-center">
            <Button>Top</Button>
        </div>
    )
}
export default ScrollToTop;