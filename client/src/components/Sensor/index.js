import React,{ Component} from 'react';
import {Link} from 'react-router-dom';
import {SensorConsumer} from "../context"
import LogDetail from '../../containers/LogDetail';

class Sensor extends Component{
    render(){
        const{sensorID,moisture,threshold}=this.props.sensor;
        return (
            
            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                
                <a href="#" class="thumbnail">
                <SensorConsumer>
                {value =>{
                    return (
                        <div className="img-container p-5"
                        onClick={()=>value.handleDetail(sensorID) }>
                     
                        <p> Name: Sensor {sensorID}</p>
                      
                        <p>Threshold: {threshold}</p>
                        <Link to="/logDetail">
                            <button className="detail-btn" >
                            LogDetail  
                            </button>   
                        </Link>
                        <Link to="/Config">
                            <button className="detail-btn" >
                            Config  
                            </button>   
                        </Link>
                        </div>)
                }}  
                </SensorConsumer>
                </a>
            </div>
        );
    }
}

export default Sensor;
