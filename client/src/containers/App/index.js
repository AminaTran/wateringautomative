
import React from "react";
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";
import Users from "../LogDetail";
import Header from "../../components/Header";
import './App.css'
import ConfigPage_01 from "../../components/Config/ConfigPage_01"
import SensorList from "../../components/SensorList"
import LogDetail from "../LogDetail";
//thamadd
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';




const App = () => (
    
  <BrowserRouter>
  <div>
    {/* <Link to='logDetail'>LogDetail</Link> */}

  <Header/>
    <Switch>
      <Route path="/header" component={Header} />
      <Route path="/config" component={ConfigPage_01} />
      <Route path="/viewLog" component={SensorList} />
      <Route path="/logDetail" component={LogDetail} />
    </Switch>
  </div> 
  </BrowserRouter>

);

export default App;



